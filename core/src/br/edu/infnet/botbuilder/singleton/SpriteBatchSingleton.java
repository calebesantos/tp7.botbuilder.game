package br.edu.infnet.botbuilder.singleton;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SpriteBatchSingleton {
	
	private SpriteBatchSingleton() {}
	
	private static class SpriteBatchInstance {
		private static final SpriteBatch INSTANCE = new SpriteBatch();
	}
	
	public static SpriteBatch getInstance() {
		return SpriteBatchInstance.INSTANCE;
	}
}
