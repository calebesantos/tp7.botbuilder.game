package br.edu.infnet.botbuilder.singleton;

import br.edu.infnet.botbuilder.tools.InputProcessorManager;

public class InputProcessorManagerSingleton {
	
	private InputProcessorManagerSingleton() {}
	
	private static class InputProcessorManagerInstance {
		private static final InputProcessorManager INSTANCE = new InputProcessorManager();
	}
	
	public static InputProcessorManager getInstance() {
		return InputProcessorManagerInstance.INSTANCE;
	}
}
