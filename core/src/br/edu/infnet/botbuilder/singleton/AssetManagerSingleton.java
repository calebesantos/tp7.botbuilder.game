package br.edu.infnet.botbuilder.singleton;

import com.badlogic.gdx.assets.AssetManager;

public class AssetManagerSingleton {
	
	private AssetManagerSingleton() {}
	
	private static class AssetManagerInstance {
		private static final AssetManager INSTANCE = new AssetManager();
	}
	
	public static AssetManager getInstance() {
		return AssetManagerInstance.INSTANCE;
	}
}
