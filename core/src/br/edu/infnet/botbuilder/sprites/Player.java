package br.edu.infnet.botbuilder.sprites;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import br.edu.infnet.botbuilder.constants.BitCollisionConstant;
import br.edu.infnet.botbuilder.constants.ScreenConstant;
import br.edu.infnet.botbuilder.factory.AnimationFactory;
import br.edu.infnet.botbuilder.screens.PlayScreen;
import br.edu.infnet.botbuilder.singleton.InputProcessorManagerSingleton;
import br.edu.infnet.botbuilder.tools.AttackHandler;
import br.edu.infnet.botbuilder.tools.ComboHandler;
import br.edu.infnet.botbuilder.tools.InputProcessorManager;

public class Player extends Sprite {

	public enum State {
		STANDING, JUMPING, RUNNING, FALLING, DEAD, ATTACKING
	};

	public State currentState;
	public State previousState;

	private World world;
	private Body body;

	private float stateTimer;
	private boolean runningRight;

	private AnimationFactory animationFactory;
	private AttackHandler attackHandler;
	private ComboHandler comboHandler;
	private InputProcessorManager inputProcessorManager;

	private HashMap<String, Animation> combos;

	private Animation ryuStand;
	private Animation ryuWalking;
	private Animation ryuJump;
	private Animation ryuPunch;
	private Animation ryuKick;

	public Player(PlayScreen screen) {
		this.world = screen.getWorld();

		inputProcessorManager = InputProcessorManagerSingleton.getInstance();

		currentState = State.STANDING;
		previousState = State.STANDING;
		stateTimer = 0;
		runningRight = true;

		animationFactory = new AnimationFactory();
		attackHandler = new AttackHandler();

		combos = new HashMap<>();
		generateCombos();

		comboHandler = new ComboHandler(combos);

		ryuStand = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_idle.png", 0, 0, 50, 82, 0.1f);
		ryuWalking = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_walking.png", 0, 0, 49, 81, 0.1f);
		ryuJump = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_jump.png", 0, 0, 42, 90, 0.1f);
		ryuPunch = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_punching.png", 0, 0, 55, 81,
				0.1f);
		ryuKick = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_kick.png", 0, 0, 63, 87, 0.15f);

		attackHandler.setAttack(Input.Keys.Z, ryuPunch);
		attackHandler.setAttack(Input.Keys.X, ryuKick);

		inputProcessorManager.addInputProcessor(attackHandler);
		inputProcessorManager.addInputProcessor(comboHandler);

		defineBodyPlayer(new Vector2(ScreenConstant.toPPM(ScreenConstant.V_WIDTH / 4), ScreenConstant.toPPM(32)));

		setBounds(0, 0, ScreenConstant.toPPM(50), ScreenConstant.toPPM(82));
		setRegion(ryuStand.getKeyFrames()[0]);
	}

	private void defineBodyPlayer(Vector2 position) {

		BodyDef bDef = new BodyDef();

		bDef.position.set(position);
		bDef.type = BodyType.DynamicBody;
		body = world.createBody(bDef);

		FixtureDef fixtureDef = new FixtureDef();
//		CircleShape shape = new CircleShape();
//
//		shape.setRadius(ScreenConstant.toPPM(6));
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(ScreenConstant.toPPM(52) / 2, ScreenConstant.toPPM(80) / 2);

		fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_BIT;
		fixtureDef.filter.maskBits = BitCollisionConstant.GROUND_BIT;

		fixtureDef.shape = shape;
		body.createFixture(fixtureDef).setUserData(this);

		body.createFixture(fixtureDef).setUserData(this);
	}

	public void update(float delta) {

		handleInput(delta);

		setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);

		comboHandler.update(delta, stateTimer);
		setRegion(getFrame());

		stateTimer = currentState == previousState ? stateTimer + delta : 0;
		previousState = currentState;
	}

	public Body getBody() {
		return body;
	}

	public TextureRegion getFrame() {
		currentState = getState();

		TextureRegion region;
		switch (currentState) {
		case ATTACKING:
			region = comboHandler.isComboDetected() ? comboHandler.getCurrentAnimation().getKeyFrame(stateTimer)
					: attackHandler.getCurrentAnimation().getKeyFrame(stateTimer);
			break;
		case JUMPING:
			region = ryuJump.getKeyFrame(stateTimer, true);
			break;
		case RUNNING:
			region = ryuWalking.getKeyFrame(stateTimer, true);
			break;
		default:
			region = ryuStand.getKeyFrame(stateTimer, true);
		}

		if ((body.getLinearVelocity().x < 0 || !runningRight) && !region.isFlipX()) {
			region.flip(true, false);
			runningRight = false;
		} else if ((body.getLinearVelocity().x > 0 || runningRight) && region.isFlipX()) {
			region.flip(true, false);
			runningRight = true;
		}

		return region;
	}

	public State getState() {

		if (attackHandler.isAttacking(stateTimer) || comboHandler.isComboDetected())
			return State.ATTACKING;

		if (body.getLinearVelocity().y > 0 || (body.getLinearVelocity().y < 0 && previousState == State.JUMPING))
			return State.JUMPING;

		if (body.getLinearVelocity().y < 0)
			return State.FALLING;

		if (body.getLinearVelocity().x != 0)
			return State.RUNNING;

		return State.STANDING;
	}

	private void handleInput(float delta) {

		if (currentState != State.DEAD) {
			
			if (Gdx.input.isKeyJustPressed(Input.Keys.UP))
				body.applyLinearImpulse(new Vector2(0, 3.5f), body.getWorldCenter(), true);

			else if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && body.getLinearVelocity().x >= -1.5)
				body.applyLinearImpulse(new Vector2(-0.5f, 0), body.getWorldCenter(), true);

			else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && body.getLinearVelocity().x <= 1.5)
				body.applyLinearImpulse(new Vector2(0.5f, 0), body.getWorldCenter(), true);
		}
	}

	private void generateCombos() {

		// Hadouken
		StringBuilder hadoukenComboKeys = new StringBuilder();
		hadoukenComboKeys.append(Input.Keys.DOWN);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.RIGHT);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.Z);

		Animation ryuHadouken = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_punching.png", 0,
				0, 58, 85, 0.1f);

		combos.put(hadoukenComboKeys.toString(), ryuHadouken);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(Input.Keys.DOWN);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.LEFT);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.Z);

		ryuHadouken = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_punching.png", 0, 0, 58, 85,
				0.1f, true, false);

		combos.put(hadoukenComboKeys.toString(), ryuHadouken);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(Input.Keys.DOWN);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.RIGHT);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.X);

		ryuHadouken = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_kick.png", 0, 0, 58, 85,
				0.1f, true, false);

		combos.put(hadoukenComboKeys.toString(), ryuHadouken);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(Input.Keys.DOWN);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.LEFT);
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(Input.Keys.X);

		ryuHadouken = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_kick.png", 0, 0, 58, 85,
				0.1f, true, false);

		combos.put(hadoukenComboKeys.toString(), ryuHadouken);
	}
}
