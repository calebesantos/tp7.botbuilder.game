package br.edu.infnet.botbuilder;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.edu.infnet.botbuilder.screens.PlayScreen;
import br.edu.infnet.botbuilder.singleton.AssetManagerSingleton;
import br.edu.infnet.botbuilder.singleton.InputProcessorManagerSingleton;
import br.edu.infnet.botbuilder.singleton.SpriteBatchSingleton;

public class BotBuilder extends Game {
	
	private AssetManager assetManager;
	private SpriteBatch spriteBatch;
	
	@Override
	public void create() {
		
		spriteBatch = SpriteBatchSingleton.getInstance();
		
		assetManager = AssetManagerSingleton.getInstance();
		assetManager.finishLoading();
		
		Gdx.input.setInputProcessor(InputProcessorManagerSingleton.getInstance());
		setScreen(new PlayScreen());
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
		assetManager.dispose();
		spriteBatch.dispose();
	}
}
