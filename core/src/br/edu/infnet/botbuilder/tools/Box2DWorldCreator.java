package br.edu.infnet.botbuilder.tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import br.edu.infnet.botbuilder.constants.ScreenConstant;
import br.edu.infnet.botbuilder.screens.PlayScreen;

public class Box2DWorldCreator {

	private TiledMap map;
	
	public Box2DWorldCreator(PlayScreen screen) {
		World world = screen.getWorld();
		map = screen.getMap();

		BodyDef bodyDef = new BodyDef();
		PolygonShape shape = new PolygonShape();
		FixtureDef fixtureDef = new FixtureDef();
		Body body;

		// Box2D create grounds
		for (MapObject mapObject : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {

			Rectangle rectangle = ((RectangleMapObject) mapObject).getRectangle();

			bodyDef.type = BodyType.StaticBody;
			
			bodyDef.position.set(ScreenConstant.toPPM(rectangle.getX() + rectangle.getWidth() / 2),
					ScreenConstant.toPPM(rectangle.getY() + rectangle.getHeight() / 2));

			body = world.createBody(bodyDef);
			
			shape.setAsBox(ScreenConstant.toPPM(rectangle.getWidth() / 2),
					ScreenConstant.toPPM(rectangle.getHeight() / 2));
			
			fixtureDef.shape = shape;
			body.createFixture(fixtureDef);
		}
	}
	
	public Rectangle getGround() {
		MapObject mapObject = map.getLayers().get(2).getObjects().get(0);
		return ((RectangleMapObject) mapObject).getRectangle();
	}
}
