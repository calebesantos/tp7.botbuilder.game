package br.edu.infnet.botbuilder.tools;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;

public class AttackHandler implements InputProcessor {

	private HashMap<Integer, Animation> mapAttack;
	private Animation animationAttack;
	private int lastKeyAttack;
	private int previousKeyAttack;
	private int currentKeyAttack;

	public AttackHandler() {
		mapAttack = new HashMap<>();
	}

	public void setAttack(int a, Animation animation) {
		mapAttack.put(a, animation);
	}

	public Animation getAnimation(Input.Keys key) {
		return mapAttack.get(key);
	}

	public Animation getCurrentAnimation() {
		return animationAttack;
	}

	public boolean isAttacking(float stateTime) {

		if (currentKeyAttack == 0 && animationAttack != null && animationAttack.isAnimationFinished(stateTime)) {
			previousKeyAttack = 0;
		}

		return currentKeyAttack > 0 || (previousKeyAttack == lastKeyAttack && animationAttack != null
				&& !animationAttack.isAnimationFinished(stateTime));
	}

	@Override
	public boolean keyDown(int keycode) {

		for (Entry<Integer, Animation> entry : mapAttack.entrySet()) {
			if (keycode == entry.getKey()) {

				currentKeyAttack = keycode;
				previousKeyAttack = keycode;
				lastKeyAttack = keycode;

				animationAttack = entry.getValue();

				return false;
			}
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		currentKeyAttack = 0;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
