package br.edu.infnet.botbuilder.factory;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class AnimationFactory {

	public Animation getAnimation(String path, int x, int y, int width, int height, float duration, boolean flipX,
			boolean flipY) {

		Array<TextureRegion> frames = new Array<TextureRegion>();

		Texture texture = new Texture(path);
		int length = texture.getWidth() / width;

		for (int i = x; i < length; i++) {
			TextureRegion region = new TextureRegion(texture, i * width, y, width, height);
			region.flip(flipX, flipY);
			frames.add(region);
		}

		return new Animation(duration, frames);
	}

	public Animation getAnimation(String path, int x, int y, int width, int height, float duration) {

		Array<TextureRegion> frames = new Array<TextureRegion>();

		Texture texture = new Texture(path);
		int length = texture.getWidth() / width;

		for (int i = x; i < length; i++) {
			frames.add(new TextureRegion(texture, i * width, y, width, height));
		}

		return new Animation(duration, frames);
	}

	public Animation getAnimation(String path, int x, int y, int width, int height) {

		float duration = 0.1f;

		Array<TextureRegion> frames = new Array<TextureRegion>();

		Texture texture = new Texture(path);
		int length = texture.getWidth() / width;

		for (int i = x; i < length; i++) {
			frames.add(new TextureRegion(texture, i * width, y, width, height));
		}

		return new Animation(duration, frames);
	}

}
