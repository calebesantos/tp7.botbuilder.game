package br.edu.infnet.botbuilder.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.net.HttpStatus;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import br.edu.infnet.botbuilder.constants.ScreenConstant;
import br.edu.infnet.botbuilder.singleton.InputProcessorManagerSingleton;
import br.edu.infnet.botbuilder.singleton.SpriteBatchSingleton;

public class Hud implements Disposable {

	private Stage stage;
	private Viewport viewport;
	private Label textLabel;

	public Hud() {

		viewport = new FitViewport(ScreenConstant.V_WIDTH, ScreenConstant.V_HEIGHT, new OrthographicCamera());
		stage = new Stage(viewport, SpriteBatchSingleton.getInstance());

		InputProcessorManagerSingleton.getInstance().addInputProcessor(stage);

		Table table = new Table();
		table.top();
		table.setFillParent(true);

		String tutorial = "Como jogar: Setas movimentam personagem. Z - Soco. X - Chute";

		textLabel = new Label(tutorial, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		table.add(textLabel).expandX().padTop(10);

		table.row();

		tutorial = "Combos: Baixo, Direita/Esquerda + Z - Soco forte. Baixo, Direita/Esquerda + X - Chute forte.";
		textLabel = new Label(tutorial, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		table.add(textLabel).expandX().padTop(10);

		table.row();

		TextButton button;

		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.font = new BitmapFont();
		textButtonStyle.fontColor = Color.BLACK;
		button = new TextButton("Clique aqui para voltar ao site", textButtonStyle);

		button.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

				// http://localhost:20759/BotBuilder/SairPartida

				final Json json = new Json();

				String requestJson = "";

				Net.HttpRequest request = new Net.HttpRequest(HttpMethods.GET);
				final String url = "http://localhost:20759/BotBuilder/SairPartida";
				request.setUrl(url);

				request.setContent(requestJson);

				request.setHeader("Content-Type", "application/json");
				request.setHeader("Accept", "application/json");

				Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {

					public void handleHttpResponse(Net.HttpResponse httpResponse) {

						int statusCode = httpResponse.getStatus().getStatusCode();
						if (statusCode != HttpStatus.SC_OK) {
							System.out.println("Request Failed");
							return;
						}

						String responseJson = httpResponse.getResultAsString();
						try {

							// DO some stuff with the response string

						} catch (Exception exception) {

							exception.printStackTrace();
						}
					}

					public void failed(Throwable t) {
						System.out.println("Request Failed Completely");
					}

					@Override
					public void cancelled() {
						System.out.println("request cancelled");

					}

				});

				return super.touchDown(event, x, y, pointer, button);
			}
		});

		table.add(button).expandX().padTop(20);

		stage.addActor(table);
	}

	public Stage getStage() {
		return stage;
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void update(float delta) {
		// TODO Auto-generated method stub

	}

}
