package br.edu.infnet.botbuilder.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import br.edu.infnet.botbuilder.constants.ScreenConstant;
import br.edu.infnet.botbuilder.scenes.Hud;
import br.edu.infnet.botbuilder.singleton.SpriteBatchSingleton;
import br.edu.infnet.botbuilder.sprites.Player;
import br.edu.infnet.botbuilder.tools.Box2DWorldCreator;
import br.edu.infnet.botbuilder.tools.WorldContactListener;

public class PlayScreen implements Screen {

	private OrthographicCamera camera;
	private Viewport viewport;
	private Hud hud;

	// TiledMap variables
	private TmxMapLoader mapLoader;
	private TiledMap map;
	private OrthogonalTiledMapRenderer mapRenderer;

	// Box2d variables
	private World world;
	private Box2DDebugRenderer box2dDebug;
	
	@SuppressWarnings("unused")
	private Box2DWorldCreator worldCreator;

	// Sprites
	private Player player;

	// SpriteBatch
	private SpriteBatch batch;

	public PlayScreen() {

		batch = SpriteBatchSingleton.getInstance();

		camera = new OrthographicCamera();
		viewport = new FitViewport(ScreenConstant.toPPM(ScreenConstant.V_WIDTH) / 4,
				ScreenConstant.toPPM(ScreenConstant.V_HEIGHT) / 4, camera);
		hud = new Hud();

		mapLoader = new TmxMapLoader();
		map = mapLoader.load("botbuilder/level1.tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(map, ScreenConstant.toPPM(1));
		camera.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);

		world = new World(new Vector2(0, -10), true);
		box2dDebug = new Box2DDebugRenderer();
		worldCreator = new Box2DWorldCreator(this);
		player = new Player(this);

		world.setContactListener(new WorldContactListener());
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {
		update(delta);

		clearScreen();

		mapRenderer.render();
//		box2dDebug.render(world, camera.combined);

		batch.setProjectionMatrix(camera.combined);

		batch.begin();

		player.draw(batch);

		batch.end();

		batch.setProjectionMatrix(hud.getStage().getCamera().combined);
		hud.getStage().draw();
	}

	public TiledMap getMap() {
		return map;
	}

	public World getWorld() {
		return world;
	}

	private void clearScreen() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	private void update(float delta) {
		
		world.step(1 / 60f, 3, 2);
		
		player.update(delta);
		
		hud.update(delta);

		float offsetBeginX = player.getBody().getPosition().x - camera.viewportWidth / 2;
		float offsetEndX = player.getBody().getPosition().x + camera.viewportWidth / 2;

		if(offsetBeginX > 0 && offsetEndX < ScreenConstant.toPPM(ScreenConstant.V_WIDTH / 1.95f))
			camera.position.x = player.getBody().getPosition().x;
		
		camera.update();
		
		mapRenderer.setView(camera);
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		map.dispose();
		world.dispose();
		box2dDebug.dispose();
		mapRenderer.dispose();
		hud.dispose();
	}

}
